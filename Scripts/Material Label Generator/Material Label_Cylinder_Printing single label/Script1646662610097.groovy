import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Windows.startApplicationWithTitle('C:\\Program Files (x86)\\Industrial Scientific Corp\\Material Label Generator\\MaterialLabelGenerator.exe', 
    'Material Label Generator')

Windows.getText(findWindowsObject('Object Repository/Material Label Generator/Text'))

Windows.click(findWindowsObject('Material Label Generator/PrintingButton'))

Windows.waitForElementPresent(findWindowsObject('Material Label Generator/PrinterText'), 0)

Windows.click(findWindowsObject('Material Label Generator/PrinterDropDown'))

Windows.verifyElementPresent(findWindowsObject('Material Label Generator/Bullzip PDF Printer'), 0)

Windows.doubleClick(findWindowsObject('Material Label Generator/Bullzip PDF Printer'))

Windows.delay(2)

Windows.click(findWindowsObject('Material Label Generator/SaveChangesButton'))

Windows.delay(2)

Windows.click(findWindowsObject('Material Label Generator/SubLabelTypeDropDown'))

Windows.waitForElementPresent(findWindowsObject('Material Label Generator/CylinderLabel'), 0)

Windows.click(findWindowsObject('Material Label Generator/CylinderLabel'))

Windows.waitForElementPresent(findWindowsObject('Material Label Generator/PartOrderNumberTextBox'), 0)

for (def rowNum = 1; rowNum <= findTestData('Data1').getRowNumbers(); rowNum++) {
    //Windows.setText(findWindowsObject('Material Label Generator/PartNumberTextbox'), PartNumber)
    Windows.setText(findWindowsObject('Material Label Generator/PartOrderNumberTextBox'), findTestData('Data1').getValue(
            1, rowNum))

    //Windows.setText(findWindowsObject('WindowsApp-Material Label/QuantityTextBox'), Quantity)
    Windows.setText(findWindowsObject('Material Label Generator/ItemNumberTextBox'), findTestData('Data1').getValue(2, rowNum))

    Windows.click(findWindowsObject('Material Label Generator/CylinderLoadButton'))

    Windows.waitForElementPresent(findWindowsObject('Object Repository/Material Label Generator/Print'), 2)

    Windows.click(findWindowsObject('Material Label Generator/CylinderDataGrid'))

    Windows.waitForElementPresent(findWindowsObject('Material Label Generator/POLabelPreview'), 0)

    Windows.click(findWindowsObject('Object Repository/Material Label Generator/Print'))

    Thread.sleep(3000)
}

Windows.closeApplication()

